var LambdaWorks = LambdaWorks || new Object();


//-- Chrome etc. compatibility
Array.map     = Array.map || function (a, f) {return a.map(f);};
Array.forEach = Array.forEach || function (a, f) {return a.forEach(f);};
Array.filter  = Array.filter || function (a, f) {return a.filter(f);};

(function () {
  //-- namespace Functional
  var Functional = new Object();

  //-- Basic utility functions.
  // 'a1 * 'a2 * 'a3 * ... * 'an -> ('a1 -> 'a2 -> ... -> 'an)
  Functional.curry = function (f) {
    if (f.length <= 1) 
      return f; 
    return x => curry(f.bind(this, x)); 
  };
  var curry = Functional.curry;

  // 'a -> 'b -> 'b -> 'c -> 'a -> 'c
  Functional.compose = f => g => x => g(f(x))
  var compose = Functional.compose;

  //-- Array functions.
  // int -> int -> 'a -> 'a array
  Functional.Array = new Object();
  Functional.Array.init = n => f => {
    var result = new Array();
    for (var i = 0; i < n; ++i) {
      result.push (f(i));
    }
    return result;
  };

  // 'a -> 'b -> 'a array -> 'b array
  Functional.Array.map = f => a => Array.map(a, f);

  // int -> 'a -> 'b -> 'a array -> 'b array
  Functional.Array.mapi = f => a => Array.map(a, function (x, i) { return f (i) (x); });

  // 'a -> bool -> 'a array -> 'a array
  Functional.Array.filter = f => a => Array.filter(a, f);

  // 'a -> 'b -> 'a array -> null 
  Functional.Array.iter = f => a => {
    Array.forEach(a, f);
    return null;
  };

  
  // 'a -> 'b -> 'a array -> null
  Functional.Array.iterBack = f => a => {
    for (var i = a.length - 1; i >= 0; --i) {
      f(a[i]);
    }
    return null;
  };


  // 'a -> 'b -> 'a array -> 'b * ('a array) array
  Functional.Array.groupBy = f => a => {
    var hashMap = new Object();
    var result  = new Array();
    var collect = Functional.Array.iter (function (x) {
      var key = f(x);
      if (hashMap[key] == undefined) {
        hashMap[key] = {0: key, 1: [x]};
      } else {
        hashMap[key][1].push (x)
      }
    });
    collect (a);
    for (var key in hashMap) {
      result.push (hashMap[key]);
    }
    return result;
  };


  //  'a -> 'b -> 'a -> 'b list -> 'a
  Functional.Array.fold = f => acc => arr => {
    var ac = acc;
    Functional.Array.iter (x => ac = f (ac) (x)) (arr);
    return ac;
  };

  // 'a -> 'b -> 'a -> 'b list -> 'a
  Functional.Array.foldBack = f => arr => acc => {
    var ac = acc;
    Functional.Array.iterBack (x => ac = f (x) (ac)) (arr);
    return ac;
  };
  
  //-- Statistical Functions
  Functional.Statistics = new Object();

  // number array -> number
  Functional.Statistics.sum = Functional.Array.fold (a => x => a + x) (0);

  // number array -> number
  Functional.Statistics.max = inArr => Functional.Array.fold (a => x => Math.max (a, x)) (inArr[0]) (inArr);


  // number array -> number
  Functional.Statistics.min = inArr => Functional.Array.fold (a => x => Math.min (a, x)) (inArr[0]) (inArr);

  // number array -> number
  Functional.Statistics.popMean = inArr => (Functional.Statistics.sum (inArr)) / inArr.length;

  // number array -> number
  Functional.Statistics.popVar = inArr => {
    var my        = Functional.Statistics.popMean (inArr);
    var sumOffset = compose (Functional.Array.map (x => Math.pow(x - my, 2))) (Functional.Statistics.sum);
    return sumOffset(inArr) / inArr.length;
  };

  // number array -> number
  Functional.Statistics.popStd = compose (Functional.Statistics.popVar) (Math.sqrt) 
  //-- String functions
  Functional.String = new Object();

  // string -> string -> string array
  Functional.String.split = tokenStr => inStr => inStr.split(tokenStr);

  // regex -> string -> string -> string
  Functional.String.replace = regex => replaceStr => inStr => inStr.replace(regex, replaceStr);

  // string -> string -> int
  Functional.String.find = searchStr => inStr => inStr.indexOf(searchStr);

  // string -> string array -> string
  Functional.String.join = delimStr => inArr => Functional.Array.fold (a => s => a + delimStr + s) (inArr[0]) (inArr.slice(1));

  //-- Extend Lambdaworks namespace.
  LambdaWorks.Functional = Functional;
}) ();
